import math
from typing import List, Union

import torch
import torchaudio
import torchaudio.functional as F


class BaseTransformer:
    """A base class for transforming inputs."""

    def transform(self, speech):
        raise NotImplementedError


class RIRTransformer(BaseTransformer):
    """Room impulse response transformer.

    Attributes:
        rir_wav_path: Room impulse response wave data path.
        impulse_ratio: Value to control how much reverb should be convolved (0-1).
        speech_sample_rate: Input speech sample rate at transform function.
    """
    def __init__(
            self,
            rir_wav_path: str,
            impulse_ratio: float = 0.25,
            speech_sample_rate: Union[None, int] = None):
        self.rir, self.sample_rate = read_wav_file(rir_wav_path, speech_sample_rate)
        self.impulse_ratio = impulse_ratio
        assert 0 < self.impulse_ratio <= 1
        self._process_rir_wave()

    def _process_rir_wave(self):
        """Extract the main impulse, normalize the signal power, flip the time axis."""
        self.rir = self.rir[:, int(self.sample_rate * 1.11):int(self.sample_rate * 1.2)]
        self.rir = self.rir / self.rir.max()
        self.rir = torch.pow(self.rir, int(1/self.impulse_ratio))
        self.rir = torch.flip(self.rir, [1])

    def transform(self, speech):
        speech_ = torch.nn.functional.pad(speech, (self.rir.shape[1] - 1, 0))
        speech = torch.nn.functional.conv1d(speech_[None, ...], self.rir[None, ...])[0]
        return speech


class BackGroundNoiseTransformer(BaseTransformer):
    """Background noise transformer.

    Attributes:
        bg_noise_wav_path: background noise wave data path.
        snr_db: Signal-to-noise ratio expressed in decibels value.
        speech_sample_rate: Input speech sample rate at transform function.
    """
    def __init__(
            self, bg_noise_wav_path: str,
            snr_db: int,
            speech_sample_rate: Union[None, int] = None
    ):
        self.noise, self.sample_rate = read_wav_file(
            bg_noise_wav_path, speech_sample_rate
        )
        self.snr_db = snr_db

    def transform(self, speech):
        if self.noise.shape[1] < speech.shape[1]:
            # Currently, noise data has only 5 second, so we need to duplicate noise to
            # map with speech input length.
            rate = math.ceil(speech.shape[1]/self.noise.shape[1])
            self.noise = torch.cat(rate*[self.noise], dim=1)
        noise = self.noise[:, :speech.shape[1]]
        scale = math.exp(self.snr_db / 10) * noise.norm(p=2) / speech.norm(p=2)
        return (scale * speech + noise) / 2


class SoxEffectTransformer(BaseTransformer):
    """Sox effects transformer.

    Details about sox effects: http://sox.sourceforge.net/sox.html#EFFECTS
    https://pytorch.org/audio/stable/sox_effects

    Attributes:
        speech_sample_rate: Input speech sample rate at transform function.
    """
    def __init__(self, speech_sample_rate: int, **kwargs):
        self.kwargs = kwargs
        self._validate_kwargs()
        self.effects = self._generate_sox_effects()
        self.speech_sample_rate = speech_sample_rate

    def _validate_kwargs(self):
        valid_effects = [
            'allpass', 'band', 'bandpass', 'bandreject', 'bass', 'bend', 'biquad',
            'chorus', 'channels', 'compand', 'contrast', 'dcshift', 'deemph', 'delay',
            'dither', 'divide', 'downsample', 'earwax', 'echo', 'echos', 'equalizer',
            'fade', 'fir', 'firfit', 'flanger', 'gain', 'highpass', 'hilbert',
            'loudness', 'lowpass', 'mcompand', 'norm', 'oops', 'overdrive', 'pad',
            'phaser', 'pitch', 'rate', 'remix', 'repeat', 'reverb', 'reverse', 'riaa',
            'silence', 'sinc', 'speed', 'stat', 'stats', 'stretch', 'swap', 'synth',
            'tempo', 'treble', 'tremolo', 'trim', 'upsample', 'vad', 'vol'
        ]
        for key in self.kwargs.keys():
            if key not in valid_effects:
                raise ValueError(f"{key} is not a sox effect.")

    def _generate_sox_effects(self) -> List[List[str]]:
        effects: List[List[str]] = []
        for effect_name, value in self.kwargs.items():
            if isinstance(value, list):
                effects.append([effect_name] + value)
            elif isinstance(value, str):
                effects.append([effect_name] + [value])
            else:
                raise ValueError(
                    f"Value of effect must be a list or string. Found {type(value)} at "
                    f"{effect_name} effect."
                )
        return effects

    def transform(self, speech):
        speech, _ = torchaudio.sox_effects.apply_effects_tensor(
            speech,
            self.speech_sample_rate,
            effects=self.effects
        )
        return speech


class CodecTransformer(BaseTransformer):
    """Codecs Transformer.

    Attributes:
        speech_sample_rate: Input speech sample rate at transform function.
        format: Codec format to transform.
    """
    def __init__(self, speech_sample_rate: int, format: str):
        self.speech_sample_rate = speech_sample_rate
        self.format = format

    def transform(self, speech):
        return F.apply_codec(speech, self.speech_sample_rate, format=self.format)


class TransformPipeline:
    """A pipeline that applies a list of transformers serially."""

    def __init__(self, transformers: List):
        self.transformers = transformers

    def transform(self, speech):
        for i in range(len(self.transformers)):
            print(f"Transfoming {self.transformers[i].__class__.__name__} ...")
            speech = self.transformers[i].transform(speech)
        return speech


def read_wav_file(wav_file_path: str, sample_rate: Union[None, int] = None):
    effects = [
        ["remix", "1"]  # Select and mix input audio channels into output audio channels
    ]
    if sample_rate:
        effects.append(["rate", f'{sample_rate}'])
    return torchaudio.sox_effects.apply_effects_file(wav_file_path, effects=effects)


def save_wav_file(wav, save_path: str, sample_rate: int, format: str = "wav") -> None:
    torchaudio.save(
        f"{save_path}.{format}", wav, sample_rate=sample_rate, format=format
    )


if __name__ == '__main__':
    raw_sample_rate = 16000
    gsm_sample_rate = 8000
    wav, sample_rate = read_wav_file(
        "data/29d062fcd6404ea98ed41fefe11b828c.wav", sample_rate=raw_sample_rate
    )
    transformers = [
        RIRTransformer(
            rir_wav_path="data/room_impulse_response.wav",
            impulse_ratio=0.5,
            speech_sample_rate=sample_rate
        ),
        BackGroundNoiseTransformer(
            bg_noise_wav_path="data/background_noise.wav",
            snr_db=30,
            speech_sample_rate=sample_rate
        ),
        SoxEffectTransformer(
            speech_sample_rate=sample_rate,
            lowpass="4000",
            compand=[
                "0.02,0.05", "-60,-60,-30,-10,-20,-8,-5,-8,-2,-8", "-8", "-7", "0.05"
            ],
            gain="-n",  # Normalize
            rate=f"{gsm_sample_rate}"
        ),
        CodecTransformer(
            speech_sample_rate=gsm_sample_rate,
            format="gsm"
        )
    ]
    prs_transformer = TransformPipeline(transformers=transformers)
    transformed_wav = prs_transformer.transform(wav)
    save_wav_file(
        transformed_wav, "data/test", sample_rate=gsm_sample_rate
    )

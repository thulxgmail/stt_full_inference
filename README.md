# Speech To Text Full Inference
Transcribe speech audio to text in real-time. 

Supported languages: Vietnamese, English.

## Installation 

- tensorflow
  
```bash
# cpu
pip install tensorflow==2.2.0
# gpu
conda install -c anaconda tensorflow-gpu==2.2.0
```

- pytorch
```bash
# cpu
pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
# gpu (CUDA 10.2)
pip install torch==1.6.0 torchvision==0.7.0
```

- packaging
```bash
pip install packaging
```
- boost, bz2, openblas, fftw3
```bash
sudo apt-get install libboost-all-dev
sudo apt-get install -y libbz2-dev
sudo apt-get install libopenblas-dev libfftw3-dev
```
- kenlm
```bash
git clone https://github.com/kpu/kenlm && \
cd kenlm && mkdir -p build && cd build && cmake .. && make -j 4
```
If still error with cmake, try
```bash
sudo apt install build-essential cmake libboost-system-dev libboost-thread-dev libboost-program-options-dev libboost-test-dev libeigen3-dev zlib1g-dev libbz2-dev liblzma-dev
```

- python bindings
```bash
(export USE_CUDA=0)  # if you use CPU
export KENLM_ROOT_DIR=path/to/kenlm
git clone -b v0.2 https://github.com/facebookresearch/wav2letter.git && \
cd wav2letter
git checkout 96f5f9
cd bindings/python && \
pip install -e .
```

- other requirements
```
sudo apt-get install python-pyaudio python3-pyaudio
pip install -r requirements.txt
pip install -e .
```

## Pre-trained models
Download all files in the links below to folder demo/

[Vietnamese](https://drive.google.com/drive/folders/1aE8pRxtenFGmk65XogkcNqrrFREikCSv?usp=sharing)

[English](https://drive.google.com/drive/folders/1CgcuHEuGQpl4_Qop_KIBB5KrLs6RFIlf?usp=sharing)


## Demo inference

```
python demo/infer.py --base_dir_vn path/to/demo
```
```
python demo/infernew.py --base_dir_vn path/to/demo
```

Some arguments:
- --base_dir_vn: path to folder "demo" where you save all pretrained models.
- --language_vn: "vietnamese" or "english". Default is "vietnamese".
- --denoise: "True" or "False". Backgournd noise suppression. Default is False.
- --w2l-decoder: type of decoding. Default is KenLM. If you don't want to use LM, set it to "viterbi".

See all arguments in demo/config.py


## Demo stream
```bash
python demo/demo_stream.py  --base_dir_vn path/to/demo 
```

## Split audio file on silence
```python
from vietnamese_wav2vec.examples.speech_recognition.splitters import Splitter
file_path = 'demo/data/audio/1.wav'
splitter = Splitter(chunk=320, sample_rate=16000, silence=0.4, max_none_frames=2,
                    max_utter=20, min_utter=5)
sub_wav_list = splitter.split(file_path, do_standardize=True,
                              save_dir=None, remove_standard=True)
```
Params:
- save_dir: if "save_dir" is not None, save audio files after split to "save_dir".
- remove_standard: if "True", remove standardize audio file after processing.

## Split audio file on silence and inference
```shell
python demo/inference_file.py --base_dir_vn path/to/demo 
```

## Add option using LM when inference
```python
# load model
_models, _generator = load_model(args, optionlm=True)
s2t_model = Infer(args, _models, _generator)
# predict using LM
texts = s2t_model.predict([wav], [sr], license='VNPT-IT-IC', optionlm=True, uselm=True)
# predict not using LM
texts = s2t_model.predict([wav], [sr], license='VNPT-IT-IC', optionlm=True, uselm=False)
```

See demo/infernew.py for detail.

## Reference
https://github.com/pytorch/fairseq/tree/master/examples/wav2vec

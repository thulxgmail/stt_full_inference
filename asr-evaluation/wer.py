#!/usr/bin/env python

# Copyright 2017-2018 Ben Lambert

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Contains the main method for the CLI.
"""

import argparse

# For some reason Python 2 and Python 3 disagree about how to import this.
try:
    from asr_evaluation.asr_evaluation import main as other_main
except Exception:
    from asr_evaluation import main as other_main

def get_parser():
    """Parse the CLI args."""
    parser = argparse.ArgumentParser(description='Evaluate an ASR transcript against a reference transcript.')

    parser.add_argument('--predict', default='/media/thulx/HDD/data/speech/ASR/correction/filter/vin/predict_valid.txt')
    parser.add_argument('--target', default='/media/thulx/HDD/data/speech/ASR/vin/metafile_vin/valid.wrd')
    parser.add_argument('--result', default='/media/thulx/HDD/data/speech/ASR/correction/filter/vin/wer_valid_vin.csv')

    parser.add_argument('--ref',  help='Reference transcript filename',
                        default=['CHỈ CẦN BÀY VỚI RUỒI TRÂU VỀ TIN QUAN TRỌNG MÀ BÉ LI'])
    parser.add_argument('--hyp', help='ASR hypothesis filename',
                        default=['CHỈ CẦN VỚI RUỒI TRÂU VỀ TIN QUAN TRỌNG MÀ BÉ LI'])
    print_args = parser.add_mutually_exclusive_group()
    print_args.add_argument('-i', '--print-instances', action='store_true',
                            help='Print all individual sentences and their errors.')
    print_args.add_argument('-r', '--print-errors', action='store_true',
                            help='Print all individual sentences that contain errors.')
    parser.add_argument('--head-ids', action='store_true',
                        help='Hypothesis and reference files have ids in the first token? (Kaldi format)')
    parser.add_argument('-id', '--tail-ids', '--has-ids', action='store_true',
                        help='Hypothesis and reference files have ids in the last token? (Sphinx format)')
    parser.add_argument('-c', '--confusions', action='store_true', help='Print tables of which words were confused.')
    parser.add_argument('-p', '--print-wer-vs-length', action='store_true',
                        help='Print table of average WER grouped by reference sentence length.')
    parser.add_argument('-m', '--min-word-count', type=int, default=1, metavar='count',
                        help='Minimum word count to show a word in confusions (default 1).')
    parser.add_argument('-a', '--case-insensitive', action='store_true',
                        help='Down-case the text before running the evaluation.')
    parser.add_argument('-e', '--remove-empty-refs', action='store_true',
                        help='Skip over any examples where the reference is empty.')

    return parser

def main():
    """Run the program."""
    parser = get_parser()
    args = parser.parse_args()
    other_main(args)

if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()

    ref = ['HẮN LĂN LÓC SAY MÊ CHÚT NHAN SẮC THỪA CỦA MẠ XIN BỎ TIỀN CHUỘC RA KHỎI TAY MỤ TRÙM CƯỚI MẠ LÀM VỢ']
    hyp = ['HẮN LĂN VỢ MÊ CHÚT NHAN SẮC THƯ CỦA MA XIN BỎ TIỀN RÚT RA KHỎI TAY MỘT TRÙM CƯỚI MAI LÀM VỢ']
    args.ref = ref
    args.hyp = hyp
    print(args.ref)
    print(args.hyp)
    wer, error_count, ref_token_count, error_detail = other_main(args)
    print(wer, error_count, ref_token_count, error_detail)

    # cnt = 0
    # with open(args.predict, 'r') as fp, open(args.target, 'r') as ft, open(args.result, 'a') as fr:
    #     fr.write('wer|error_count|ref_token_count|score|Target|Predict\n')
    #     for pre, tar in zip(fp, ft):
    #         pre = pre.split('|')  # text|score\n
    #         args.ref = [tar[:-1]]
    #         args.hyp = [pre[0]]
    #         wer, error_count, ref_token_count = other_main(args)
    #         fr.write(f'{wer*100:.3f}|{error_count}|{ref_token_count}|{pre[1][:-1]}|{args.ref[0]}|{args.hyp[0]}\n')
    #
    #         cnt += 1
    #         if cnt % 1000 == 0:
    #             print(cnt)


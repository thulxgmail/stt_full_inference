from vietnamese_wav2vec.examples.speech_recognition.infer import my_parse_args, my_make_parser, load_model, Infer
import soundfile as sf
import os


if __name__ == '__main__':
    # setup parameters
    parser = my_make_parser()
    parser.add_argument("--metadata", help="file contain all audio file names",
                        default='/home/thulx/s2t/voice-show-backend/voice_show_backend/stream_handler/min2.tsv')
    parser.add_argument("--root", help="directory contain audio files",
                        default='/home/thulx/s2t/voice-show-backend/voice_show_backend/stream_handler/min2s')
    parser.add_argument("--predict_file", help="path to saved result",
                        default='/home/thulx/s2t/voice-show-backend/voice_show_backend/stream_handler/predict.txt')
    parser.set_defaults(base_dir_vn='/home/thulx/s2t/w2v2/new_infer/decodewithlanguagemodel/stt_full_inference/demo',
                        max_sentences=2,
                        distributed_world_size=1)
    # other useful arguments:
    #   base_dir_vn: directory contain models
    #   max_sentences: batch size
    #   distributed_world_size: number of GPUs
    args = my_parse_args(parser)
    # load model
    _models, _generator = load_model(args, optionlm=True)
    s2t_model = Infer(args, _models, _generator)
    #
    if not args.metadata:
        files = [os.path.join(args.root, fn) for fn in os.listdir(args.root)]
        soundlist = []
        srlist = []
        for file in files:
            wav, sr = sf.read(file)
            soundlist.append(wav)
            srlist.append(sr)
        texts = s2t_model.predict(soundlist, srlist, license='VNPT-IT-IC', optionlm=True, uselm=True,
                                  predict_file=args.predict_file)
        print(texts)

    else:
        texts = s2t_model.predict(license='VNPT-IT-IC', optionlm=True, uselm=True,
                                  predict_file=args.predict_file, debug=False)
        # print(texts)


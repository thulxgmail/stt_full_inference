from pydub import AudioSegment, effects
import os
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--source_dir')
parser.add_argument('--target_dir')
parser.add_argument('--backgrounds_dir')

args = parser.parse_args()

# noise level decreasing
low = 10
high = 15
#
source_dir = args.source_dir
target_dir = args.target_dir
backgrounds_dir = args.backgrounds_dir

if not os.path.isdir(target_dir):
    os.makedirs(target_dir)

bg_fns = os.listdir(backgrounds_dir)

for fn in os.listdir(source_dir):
    print(fn)
    audio = AudioSegment.from_wav(os.path.join(source_dir, fn))
    audio = effects.normalize(audio)

    background = AudioSegment.from_wav(os.path.join(backgrounds_dir, np.random.choice(bg_fns)))
    # background = effects.normalize(background)

    while len(background) < len(audio):
        new_bg = AudioSegment.from_wav(os.path.join(backgrounds_dir, np.random.choice(bg_fns)))
        # new_bg = effects.normalize(new_bg)
        background = background + new_bg

    if len(background) >= len(audio):
        _start = np.random.randint(len(background) - len(audio))
        background = background[_start: _start + len(audio)]

    assert len(background) == len(audio)

    background = effects.normalize(background)
    decrease = np.random.randint(low, high)
    background = background - decrease

    new_audio = background.overlay(audio)
    new_audio.export(os.path.join(target_dir, fn) + 'noisy.wav', format='wav')
    # break


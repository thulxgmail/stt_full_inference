from vietnamese_wav2vec.examples.speech_recognition.infer import make_parser, options
import os

def set_arguments():
    parser = make_parser()
    parser.add_argument("--denoise", help="denoise audio", default=False, type=bool)
    parser.add_argument("--language_vn", help="english or vietnamese", default='vietnamese')
    parser.add_argument("--base_dir_vn", default="demo")
    parser.set_defaults(task="audio_pretraining",
                        nbest=1,
                        gen_subset="train",
                        results_path="infer_test",
                        w2l_decoder="kenlm",
                        lm_weight=2.15,
                        word_score=-0.52,
                        sil_weight=0.0,
                        criterion="ctc",
                        max_tokens=4000000,
                        post_process="letter",
                        beam=1500)
    parser.add_argument("--chunk", default=320)
    parser.add_argument("--idfile", default=0)
    parser.add_argument("--maxutter", default=5)
    parser.add_argument("--minutter", default=2)
    parser.add_argument("--maxnoneframes", default=2)
    parser.add_argument("--ARGSvad_aggressiveness", default=0)
    parser.add_argument("--ARGSsavewav", default=None)
    parser.add_argument("--ARGSrate", default=16000)
    parser.add_argument("--ARGSsilence", default=1.5)
    parser.add_argument("--ARGSfile", default=None)
    parser.add_argument("--denoise_model_path")

    args = options.parse_args_and_arch(parser)
    args.labels = 'ltr'
    args.remove_bpe = 'letter'
    args.denoise_model_path = os.path.join(args.base_dir_vn, "denoise_v1.0.h5")

    if args.language_vn == 'vietnamese':
        args.data = os.path.join(args.base_dir_vn, "data/asset_vn")
        args.path = os.path.join(args.base_dir_vn, "s2t_v1.3.pt")
        args.kenlm_model = os.path.join(args.base_dir_vn, "lm_v1.2.bin")
        args.lexicon = os.path.join(args.base_dir_vn, "lexicon_v1.2.2.txt")
    elif args.language_vn == 'english':
        args.data = os.path.join(args.base_dir_vn, "data/asset")
        args.path = os.path.join(args.base_dir_vn, "asr2_reduce.pt")
        args.kenlm_model = os.path.join(args.base_dir_vn, "4-gram.bin")
        args.lexicon = os.path.join(args.base_dir_vn, "librispeech_lexicon.lst")
    else:
        print('Only english or vietnamese speech to text model is supported so far.')

    args.maxutter = args.maxutter * args.ARGSrate
    args.minutter = args.minutter * args.ARGSrate
    args.maxnoneframes = args.maxnoneframes * args.ARGSrate // args.chunk

    args.acronym_json = os.path.join(args.base_dir_vn, "acronym_reduce.json")

    print(args.lexicon, args.w2l_decoder, args.kenlm_model)
    print('Use noise suppression:', args.denoise)
    return args

import os
import numpy as np
import soundfile as sf
import  wave
import collections, queue
import webrtcvad
import pyaudio
from scipy import signal
from halo import Halo
from vietnamese_wav2vec.examples.speech_recognition.infer import *
from vietnamese_wav2vec.noise_suppression.inference import Noise_Suppression_Inference
import os

from config import set_arguments
from vietnamese_wav2vec.stream.audio import Audio
from vietnamese_wav2vec.stream.vad import VADAudio
from vietnamese_wav2vec.examples.speech_recognition.infer import Infer


def save(sound, args):
    sf.write(
            file=os.path.join(args.ARGSsavewav, 'file' + str(args.idfile).zfill(6) + '.wav'),
            data=sound,
            samplerate=args.ARGSrate)
    print('Save audio to ', os.path.join(args.ARGSsavewav, 'file' + str(args.idfile).zfill(6) + '.wav'))
    args.idfile += 1


def main(args):
    # stream raw audio from microphone or file
    streamer = Audio(input_rate=args.ARGSrate, file=args.ARGSfile)
    frames = streamer.frame_generator()
    # load model
    _models, _generator = load_model(args)
    s2t_model = Infer(args, _models, _generator)
    if args.denoise:
        denoise_model = Noise_Suppression_Inference(args.denoise_model_path)
    ############################
    ########## Main ############
    ############################
    print("Listening (ctrl-C to exit)...")
    spinner = Halo(spinner='line')
    vad_audio = VADAudio(aggressiveness=args.ARGSvad_aggressiveness)
    frames = vad_audio.vad_collector(silence=args.ARGSsilence,    # split audio on silence
                                     frames=frames)
    vad_audio.createStream()
    triggered = True
    accum_sound = np.zeros(0)
    noneframes = 0

    for frame in frames:
        if frame is not None and len(vad_audio.stream_context) // 2 < args.maxutter:
            spinner.start()
            vad_audio.feedAudioContent(frame)
            triggered = False
        elif not triggered:
            if frame is not None:
                vad_audio.feedAudioContent(frame)
            spinner.stop()
            sound = vad_audio.finishStream()
            if len(sound) > 0:
                accum_sound = np.append(accum_sound, sound)
                if len(accum_sound) >= args.minutter:
                    if args.denoise:
                        accum_sound = denoise_model.procsee_audio(accum_sound)
                    # speech to text
                    text = s2t_model.predict([accum_sound], [args.ARGSrate], license='VNPT-IT-IC')[0]
                    print(text)  # ['VÍ DỤ ĐẦU RA']
                    if args.ARGSsavewav:
                        save(accum_sound, args)
                    if frame is not None:
                        accum_sound = accum_sound[-int(args.ARGSrate * 0.3): ]
                    else:
                        accum_sound = np.zeros(0)
            vad_audio.createStream()
            triggered = True
            noneframes = 1
        else:
            noneframes += 1
            if noneframes > args.maxnoneframes and len(accum_sound) > 0:
                if args.denoise:
                    accum_sound = denoise_model.procsee_audio(accum_sound)
                text = s2t_model.predict([accum_sound], [args.ARGSrate], license='VNPT-IT-IC')[0]
                print(text)  # ['VÍ DỤ ĐẦU RA']
                if args.ARGSsavewav:
                    save(accum_sound, args)
                accum_sound = np.zeros(0)
    if len(accum_sound) > 0: #>= args.minutter:
        if args.denoise:
            accum_sound = denoise_model.procsee_audio(accum_sound)
        # speech to text
        text = s2t_model.predict([accum_sound], [args.ARGSrate], license='VNPT-IT-IC')[0]
        print(text)  # ['VÍ DỤ ĐẦU RA']
        if args.ARGSsavewav:
            save(accum_sound, args)


if __name__ == '__main__':
    # ARGSfile=None: stream audio from microphone, if ARGSfile != None, stream audio from file "ARGSfile".
    # if ARGSsavewav != None, save audio to folder ARGSsavewav.
    args = set_arguments()
    main(args)
import argparse
import torch

parser = argparse.ArgumentParser()
parser.add_argument('--model')
parser.add_argument('--save')

args = parser.parse_args()

model = torch.load(args.model, map_location='cpu')
del model['optimizer_history']
del model['extra_state']
del model['last_optimizer_state']

pretrain = torch.load('/media/thulx/HDD/data/speech/model/300hoursfromscratch/checkpoint779.pt', map_location='cpu')
model['args'].w2v_args = pretrain['args']
torch.save(model, args.save)

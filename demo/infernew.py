from vietnamese_wav2vec.examples.speech_recognition.infer import load_model, Infer
from vietnamese_wav2vec.noise_suppression.inference import Noise_Suppression_Inference
from config import set_arguments
import soundfile as sf
import os



if __name__ == '__main__':
    args = set_arguments()
    # load model
    _models, _generator = load_model(args, optionlm=True)
    s2t_model = Infer(args, _models, _generator)
    if args.denoise:
        denoise_model = Noise_Suppression_Inference(args.denoise_model_path)

    #
    files = [os.path.join(args.base_dir_vn, 'data/audio/1.wav')]

    for i, file in enumerate(files):
        print(i, file)
        wav, sr = sf.read(file)
        # denoise
        if args.denoise:
            wav = denoise_model.procsee_audio(wav)
        # speech to text
        import time

        # use language model
        s = time.time()
        texts = s2t_model.predict([wav], [sr], license='VNPT-IT-IC', optionlm=True, uselm=True)

        print(time.time() - s)
        # print("Recognition:")
        print(texts[0])
#####################################################################################################
        # no language model
        s = time.time()
        texts = s2t_model.predict([wav], [sr], license='VNPT-IT-IC', optionlm=True, uselm=False)

        print(time.time() - s)
        # print("Recognition:")
        print(texts[0])

        print('========================')



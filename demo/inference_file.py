from vietnamese_wav2vec.examples.speech_recognition.infer import load_model, Infer
from vietnamese_wav2vec.examples.speech_recognition.splitters import Splitter
from vietnamese_wav2vec.noise_suppression.inference import Noise_Suppression_Inference
from config import set_arguments
import time

import soundfile as sf
import os



if __name__ == '__main__':
    args = set_arguments()
    # load model
    _models, _generator = load_model(args)
    s2t_model = Infer(args, _models, _generator)
    if args.denoise:
        denoise_model = Noise_Suppression_Inference(args.denoise_model_path)
    # process file
    file_path = 'demo/data/audio/savewav_2021-04-12_15-50-20_841943.wav'
    splitter = Splitter(chunk=320, sample_rate=16000, silence=0.4, max_none_frames=2,
                        max_utter=20, min_utter=5)
    sub_wav_list = splitter.split(file_path, do_standardize=True,
                                  save_dir=None, remove_standard=True)
    for wav in sub_wav_list:
        s = time.time()
        texts = s2t_model.predict([wav], [args.ARGSrate], license='VNPT-IT-IC')
        print(time.time() - s)
        print(texts[0])
        print('========================')


#!/bin/bash

python -u infer_batch.py --base_dir_vn $BASE_DIR \
--metadata $metadata \
--root $root \
--predict_file $predict_file \
--max-sentences $max_sentences \
--distributed-world-size $distributed_world_size
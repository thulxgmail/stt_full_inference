from abbreviation_converter.normalise import Converter
import time
from tqdm import tqdm
from cleaners import viet_cleaners as clean
from multiprocessing import Pool
import os

# file = '/media/thulx/HDD/data/speech/ASR/Language_model/film_transcript/train.vi'


def process_file(file):
    acro_conv = Converter()
    st = time.time()
    with open(file, 'r') as f, open(file + '_clean2.txt', 'w') as cf:
        for i, line in enumerate(f):
            if i < 396726:
                continue
            try:
                line = acro_conv.translator(line[:-1])
                line = clean(line)
                cf.write(line + "\n")
                print(i)
            except:
                print(line)
    print('time ', time.time() - st)

if __name__ == '__main__':
    # srcdir = '/mnt/DATA/thulx/language_model/data/split'
    # num_workers = 16
    #
    # # split file: split -l n_of_line source_file out_name
    # files = [os.path.join(srcdir, fn) for fn in os.listdir(srcdir)
    #          if not fn.endswith('clean.txt')]
    # print(num_workers, len(files))
    # print(files)
    #
    # with Pool(processes=num_workers) as pool:
    #     pool.map(process_file, files)

    file = '/media/thulx/HDD/data/speech/ASR/Language_model/film_transcript/train.vi'
    process_file(file)
# Training a language model

## 0. Preparation
### 0.1 Data
10M text sentences.

### 0.2 Requirements
- abbreviation_converter
- kenlm



## 1. Preprocess input text
- Convert acronyms and abbreviations
- Upper words, remove punctuations, collapse whitespaces

See preprocess.py

### Note: if input text file is too large, split it into some smaller files and process them parallel.
```python
from multiprocessing import Pool
with Pool(processes=num_workers) as pool:
    pool.map(process_file_func, list_files)
```

## 2. Build a dictionary (single word level)

## 3. Train a language model

### 3.1 Train:
```shell
kenlm/build/bin/lmplz --discount_fallback -o 4 -S 60% -T tmp <valid.wrd >lm.arpa
```
where: 
      -o: n-gram
	  -S: memory use
	  -T: tempory file
	  valid.wrd: corpus
	  lm.arpa: model

### 3.2 Convert lm.arpa(.gz) to lm.bin:
```shell
kenlm/build/bin/build_binary trie lm.arpa lm.bin
```

### 3.1 Test lm:
```shell
echo "This is my sentence ." | kenlm/build/bin/query lm.bin
```
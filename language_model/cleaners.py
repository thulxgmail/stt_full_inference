"""Vietnamese cleaner."""
import re
import string

# Regular expression matching whitespace:
_whitespace_re = re.compile(r"\s+")


def collapse_whitespace(text):
    """
    collapse whitespace in text.

    Args:
      text (str): input text

    Returns:
      text which have no multiple consecutive whitespaces
    """
    return re.sub(_whitespace_re, " ", text)


def viet_cleaners(text):
    """
    Basic pipeline that upper cases, correct tone position and collapses whitespace.

    Args:
      text (str): input text

    Returns:
      clean text
    """
    text = text.upper()
    # text = visen.clean_tone(text)
    puncs = string.punctuation + chr(8230) + chr(8220) + chr(8221) + chr(8211)
    # chr(8230) is "…", 8220 ", 8221 ", 8211 -
    text = text.translate(
        str.maketrans(puncs, " " * len(puncs))
    )  # replace punctuation by space,  chr(8230) is "…"
    text = collapse_whitespace(text)
    text = text.strip()
    return text

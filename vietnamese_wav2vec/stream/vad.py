import numpy as np
import collections
import webrtcvad

class VADAudio():
    """Filter & segment audio with voice activity detection."""

    def __init__(self, aggressiveness=3, block_per_second=50, sample_rate=16000):
        """
        Parameters
        ----------
        aggressiveness : Set aggressiveness of VAD: an integer between 0 and 3, 0 being the least aggressive
            about filtering out non-speech, 3 the most aggressive. Default: 3
        block_per_second :
        sample_rate :
        """
        self.vad = webrtcvad.Vad(aggressiveness)
        self.frame_duration_ms = 1000 // block_per_second
        self.sample_rate = sample_rate

    def vad_collector(self, frames, padding_ms=300, ratio=0.75, silence=2):
        """Generator that yields series of consecutive audio frames comprising each utterance, separated by yielding None.
            Determines voice activity by ratio of frames in padding_ms. Uses a buffer to include padding_ms prior to being triggered.
            Example: (frame, ..., frame, None, None, None, frame, ..., frame, None, ...)
                      |---utterence---|                    |---utterence---|
        """
        num_padding_frames = padding_ms // self.frame_duration_ms
        ring_buffer = collections.deque(maxlen=num_padding_frames)
        ring_buffer2 = collections.deque(maxlen=int(num_padding_frames * silence))

        triggered = False
        for frame in frames:
            if len(frame) < 640:
                yield None
                return

            is_speech = self.vad.is_speech(frame, self.sample_rate)

            if not triggered:
                yield None
                ring_buffer.append((frame, is_speech))
                num_voiced = len([f for f, speech in ring_buffer if speech])
                if num_voiced > ratio * ring_buffer.maxlen:
                    triggered = True
                    for f, s in ring_buffer:
                        yield f
                    ring_buffer.clear()

            else:
                yield frame
                ring_buffer2.append((frame, is_speech))
                num_unvoiced = len([f for f, speech in ring_buffer2 if not speech])
                if num_unvoiced > ratio * ring_buffer2.maxlen:
                    triggered = False
                    yield None
                    ring_buffer2.clear()

    def createStream(self):
        self.stream_context = bytearray()

    def feedAudioContent(self, chunk):
        self.stream_context.extend(chunk)

    def finishStream(self):
        return np.frombuffer(self.stream_context, np.int16) / 32768

from vietnamese_wav2vec.examples.speech_recognition.infer import transcribe
import numpy as np

class ASRModel():
    def __init__(self, args, model, generator, denoise_model):
        self.args = args
        self.model = model
        self.generator = generator
        self.denoise_model = denoise_model

    def createStream(self):
        self.stream_context = bytearray()

    def feedAudioContent(self, chunk):
        self.stream_context.extend(chunk)

    def finishStream(self):
        self.stream_context = np.frombuffer(self.stream_context, np.int16) / 32768
        return self.stream_context

    def predict(self, sound_list):

        sents = []
        if len(sound_list[0]) > 0:
            sents = self.speech2text(sound_list, [16000])

        return sents

    def speech2text(self, sound_list, sr_list):
        if self.args.denoise:
            for i in range(len(sound_list)):
                sound_list[i] = self.denoise_model.procsee_audio(sound_list[i])
        texts = transcribe(sound_list, sr_list, self.args, self.model,
                           self.generator, license="VNPT-IT-IC")
        return texts[0]

if __name__ == '__main__':
    # model = ASRModel(pretrain_model='/media/thulx/HDD/language_model/mailong/vietnamese_wav2vec/pretrain.pt',
    #                   finetune_model='/media/thulx/HDD/language_model/mailong/vietnamese_wav2vec/finetune.pt',
    #                   dictionary='/media/thulx/HDD/language_model/mailong/vietnamese_wav2vec/dict.ltr.txt',
    #                   lm_lexicon='/media/thulx/HDD/language_model/mailong/vietnamese_wav2vec/lexicon.txt',
    #                   lm_model='/media/thulx/HDD/language_model/mailong/vietnamese_wav2vec/lm.bin',
    #                   lm_weight=1.5, word_score=-1, beam_size=50)
    # import librosa
    # y, sr = librosa.load('/home/thulx/s2t/w2v2/stt_vn_interface/demo/data/audio/6148.wav', sr=16000)
    # x = np.empty(shape=0)
    # scores, word_scores, sents = model.predict([x, y])
    # print(sents)
    print(1)


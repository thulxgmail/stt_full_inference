import os
import sys
import datetime
end_datetime_object = datetime.datetime.strptime('12 31 2021  9:00AM', '%m %d %Y %I:%M%p')
curr_datetime_object = datetime.datetime.now()
if curr_datetime_object > end_datetime_object:
    print("Processing normally")
    sys.exit()
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import importlib
import os

from vietnamese_wav2vec.vnwav import registry
from vietnamese_wav2vec.vnwav.criterions.fairseq_criterion import FairseqCriterion, LegacyFairseqCriterion


build_criterion, register_criterion, CRITERION_REGISTRY = registry.setup_registry(
    '--criterion',
    base_class=FairseqCriterion,
    default='cross_entropy',
)


# automatically import any Python files in the criterions/ directory
for file in os.listdir(os.path.dirname(__file__)):
    if (file.endswith('.py') or file.endswith('.so')) and not file.startswith('_'):
        if file.endswith('.py'):
            module = file[:file.find('.py')]
        else:
            module = file[:file.find('.cpy')]
        importlib.import_module('vietnamese_wav2vec.vnwav.criterions.' + module)

# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.


import importlib
import os

from vietnamese_wav2vec.vnwav import registry


build_tokenizer, register_tokenizer, TOKENIZER_REGISTRY = registry.setup_registry(
    '--tokenizer',
    default=None,
)


build_bpe, register_bpe, BPE_REGISTRY = registry.setup_registry(
    '--bpe',
    default=None,
)


# automatically import any Python files in the encoders/ directory
for file in os.listdir(os.path.dirname(__file__)):
    if (file.endswith('.py') or file.endswith('.so')) and not file.startswith('_'):
        if file.endswith('.py'):
            module = file[:file.find('.py')]
        else:
            module = file[:file.find('.cpy')]
        importlib.import_module('vietnamese_wav2vec.vnwav.data.encoders.' + module)

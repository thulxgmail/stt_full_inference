# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

__all__ = ['pdb']
__version__ = '0.9.0'

import sys

# backwards compatibility to support `from vietnamese_wav2vec.vnwav.meters import AverageMeter`
from vietnamese_wav2vec.vnwav.logging import meters, metrics, progress_bar  # noqa
sys.modules['vnwav.meters'] = meters
sys.modules['vnwav.metrics'] = metrics
sys.modules['vnwav.progress_bar'] = progress_bar

import vietnamese_wav2vec.vnwav.criterions  # noqa
import vietnamese_wav2vec.vnwav.models  # noqa
import vietnamese_wav2vec.vnwav.modules  # noqa
# import vietnamese_wav2vec.vnwav.optim  # noqa
# import vietnamese_wav2vec.vnwav.optim.lr_scheduler  # noqa
# import vietnamese_wav2vec.vnwav.pdb  # noqa
# import vietnamese_wav2vec.vnwav.scoring  # noqa
import vietnamese_wav2vec.vnwav.tasks  # noqa
# import vietnamese_wav2vec.vnwav.token_generation_constraints  # noqa

# import vietnamese_wav2vec.vnwav.benchmark  # noqa
# import vietnamese_wav2vec.vnwav.model_parallel  # noqa

# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import importlib
import os


# automatically import any Python files in the models/huggingface/ directory
models_dir = os.path.dirname(__file__)
for file in os.listdir(models_dir):
    path = os.path.join(models_dir, file)
    if (
        not file.startswith('_')
        and not file.startswith('.')
        and (file.endswith('.so') or file.endswith('.py') or os.path.isdir(path))
    ):
        if file.endswith('.so'):
            model_name = file[:file.find('.cpy')] if file.endswith('.so') else file
        else:
            model_name = file[:file.find('.py')] if file.endswith('.py') else file

        module = importlib.import_module('vietnamese_wav2vec.vnwav.models.huggingface.' + model_name)

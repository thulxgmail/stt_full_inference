"""Inference module."""
import soundfile as sf
import librosa
import numpy as np
from vietnamese_wav2vec.noise_suppression.DTLN_model import DTLN_model
import os


class Noise_Suppression_Inference:
    """Inference class."""

    def __init__(self, model_path="pretrained_model/model.h5"):
        """Init method."""
        self.modelClass = DTLN_model()
        self.modelClass.build_DTLN_model()
        self.modelClass.model.load_weights(model_path)

    def procsee_audio(self, audio):
        in_data = audio
        # predict audio with the model
        predicted = self.modelClass.model.predict_on_batch(
            np.expand_dims(in_data, axis=0).astype(np.float32))
        # squeeze the batch dimension away
        predicted_speech = np.squeeze(predicted)
        return predicted_speech

    def process_file(self, audio_file_name, out_file_name):
        """
        Funtion to read an audio file, rocess it by the network and |.

        write the enhanced audio to .wav file.

        Parameters
        ----------
        model : Keras model
            Keras model, which accepts audio in the size (1,timesteps).
        audio_file_name : STRING
            Name and path of the input audio file.
        out_file_name : STRING
            Name and path of the target file.

        """
        # read audio file with librosa to handle resampling and enforce mono
        in_data, fs = librosa.core.load(audio_file_name, sr=16000, mono=True)
        # predict audio with the model
        predicted = self.modelClass.model.predict_on_batch(
            np.expand_dims(in_data, axis=0).astype(np.float32))
        # squeeze the batch dimension away
        predicted_speech = np.squeeze(predicted)
        # write the file to target destination
        sf.write(out_file_name, predicted_speech, fs)

    def process_folder(self, folder_name, new_folder_name):
        """
        Function to find .wav files in the folder and subfolders of |.

        "folder_name", process each .wav file with an algorithm and write
        it back to disk in the folder "new_folder_name". The structure of
        the original directory is preserved. The processed files will be
        saved with the same name as the original file.

        Parameters
        ----------
        model : Keras model
            Keras model, which accepts audio in the size (1,timesteps).
        folder_name : STRING
            Input folder with .wav files.
        new_folder_name : STRING
            Traget folder for the processed files.

        """
        # empty list for file and folder names
        file_names = []
        directories = []
        new_directories = []
        # walk through the directory
        for root, dirs, files in os.walk(folder_name):
            for file in files:
                # look for .wav files
                if file.endswith(".wav"):
                    # write paths and filenames to lists
                    file_names.append(file)
                    directories.append(root)
                    # create new directory names
                    new_directories.append(root.replace(folder_name,
                                                        new_folder_name))
                    # check if new_folder_name already exists,if not create it
                    if not os.path.exists(
                            root.replace(folder_name, new_folder_name)):
                        os.makedirs(root.replace(folder_name, new_folder_name))
        # iterate over all .wav files
        for idx in range(len(file_names)):
            # process each file with the model
            self.process_file(os.path.join(directories[idx], file_names[idx]),
                              os.path.join(new_directories[idx],
                                           file_names[idx]))
            print(file_names[idx] + ' processed successfully!')

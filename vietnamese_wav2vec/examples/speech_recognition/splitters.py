import os
import wave
from typing import List, Union
from datetime import datetime

import numpy as np
import soundfile as sf
import collections
import webrtcvad
from halo import Halo


class Splitter:
    def __init__(
        self,
        max_utter: int = 5,
        min_utter: int = 2,
        max_none_frames: int = 2,
        vad_aggressiveness: int = 0,
        sample_rate: int = 16000,
        silence: float = 1.5,
        chunk: int = 320,
        last: float = 0.4
    ):
        """A class for splitting a wav file to list of sub wav arrays.

        Attributes:
             max_utter: Maximum length of a utterance (second).
             min_utter: Minimum length of a utterance (second).
             max_none_frames: Maximum number of frames None between two consecutive
                concatenate utterances.
             vad_aggressiveness: Aggressiveness value of vad. An integer between 0 and 3
                0 being the least aggressive about filtering out non-speech, 3 the
                most aggressive.
            sample_rate: Input device sample rate.
            silence: A value indicating how long a silent segment.
            chunk: Chunk size.
        """
        assert min_utter > last
        self.chunk = chunk
        self.max_utter = max_utter * sample_rate
        self.min_utter = min_utter * sample_rate
        self.max_none_frames = max_none_frames * sample_rate // self.chunk
        self.vad_aggressiveness = vad_aggressiveness
        self.silence = silence
        self.sample_rate = sample_rate
        self.last = int(last * sample_rate) // self.chunk

    def _save(self, sound: np.ndarray, save_dir: str, file_idx: int) -> int:
        """Saves a wave array to a wav file."""
        file_path = os.path.join(
            save_dir, "file" + str(file_idx).zfill(6) + ".wav"
        )
        sf.write(file=file_path, data=sound, samplerate=self.sample_rate)
        print("Save audio to ", file_path)
        file_idx +=1
        return file_idx

    def _read_wav_file(self, file_path: str) -> List:
        """Reads a wav file and convert to list of bytes objects."""
        with wave.open(file_path, "rb") as w:
            frames = [
                w.readframes(self.chunk) for _ in range(
                    w.getnframes() // self.chunk + 1
                )
            ]
        return frames

    def _standardize_wav_file(self, file_path: str, out_file):
        """standardizes and saves a wav file."""
        os.system(
            f'ffmpeg -i "{file_path}" -acodec pcm_s16le -ar 16000 -ac 1 -f wav '
            f'"{out_file}" -y'
        )

    def split(
            self,
            file_path: str,
            save_dir: Union[str, None] = None,
            do_standardize: bool = False,
            remove_standard: bool = True
    ) -> List[np.ndarray]:
        """Splits a wav file to list of sub wave arrays.

        Args:
            file_path: Original wav file path.
            save_dir: name of directory to save split arrays. If save_dir is None means
            not save split arrays.
            do_standardize: A boolean value indicating standardize original wav file
                or not.
            remove_standard: remove standard wav file or not.
        """
        print("Splitting (ctrl-C to exit)...")
        if save_dir is not None and not os.path.isdir(save_dir):
            os.mkdir(save_dir)
        if do_standardize:
            if not os.path.isdir('standard_audio'):
                os.mkdir('standard_audio')
            standard_file = os.path.join('standard_audio', datetime.now().strftime("savewav_%Y-%m-%d_%H-%M-%S_%f.wav"))
            self._standardize_wav_file(file_path, out_file=standard_file)
            file_path = standard_file
        spinner = Halo(spinner='line')
        vad_audio = VADAudio(aggressiveness=self.vad_aggressiveness)
        frames = self._read_wav_file(file_path)
        if do_standardize and remove_standard:
            os.remove(file_path)
        frames = vad_audio.vad_collector(silence=self.silence, frames=frames)
        vad_audio.create_stream()
        triggered = True
        num_none_frames = 0
        accum_sound = np.zeros(0)
        accum_sound_list = []
        file_idx = 0

        for frame in frames:
            if (
                frame is not None
                and len(vad_audio.stream_context) // 2 < self.max_utter
            ):
                spinner.start()
                vad_audio.feed_audio_content(frame)
                triggered = False
            elif not triggered:
                if frame is not None:
                    vad_audio.feed_audio_content(frame)
                spinner.stop()
                sound = vad_audio.finish_stream()
                if len(sound) > 0:
                    accum_sound = np.append(accum_sound, sound)
                    if len(accum_sound) >= self.min_utter:
                        if frame is not None:
                            accum_sound, last_sound = self.split_word(accum_sound)
                        if save_dir is not None:
                            file_idx = self._save(accum_sound, save_dir=save_dir, file_idx=file_idx)
                        accum_sound_list.append(accum_sound)
                        if frame is not None:
                            accum_sound = last_sound
                        else:
                            accum_sound = np.zeros(0)

                vad_audio.create_stream()
                triggered = True
                num_none_frames = 1
            else:
                num_none_frames += 1
                if num_none_frames > self.max_none_frames and len(accum_sound) > 0:
                    if save_dir is not None:
                        file_idx = self._save(accum_sound, save_dir=save_dir, file_idx=file_idx)
                    accum_sound_list.append(accum_sound)
                    accum_sound = np.zeros(0)
        if len(accum_sound) > 0:
            if save_dir is not None:
                file_idx = self._save(accum_sound, save_dir=save_dir, file_idx=file_idx)
            accum_sound_list.append(accum_sound)

        return accum_sound_list

    def split_word(self, sound):
        def _energy(int_arr):
            avg = sum(np.abs(int_arr)) / int_arr.shape[0]
            return avg
        min_e = 32769
        idx = len(sound)
        for i in range(self.last):
            e = _energy(sound[len(sound)-self.chunk*(i+1): len(sound)-self.chunk*i])
            if e < min_e:
                min_e = e
                idx = i
        idx = len(sound)-self.chunk*(idx+1)
        return sound[: idx], sound[idx: ]


class VADAudio:
    """Filter & segment audio with voice activity detection."""

    def __init__(
            self,
            aggressiveness: int = 3,
            block_per_second: int = 50,
            sample_rate: int = 16000
    ):
        self.vad = webrtcvad.Vad(aggressiveness)
        self.frame_duration_ms = 1000 // block_per_second
        self.sample_rate = sample_rate
        self.stream_context = None

    def vad_collector(
            self, frames, padding_ms: int = 300, ratio: float = 0.75, silence: float = 2
    ):
        """Generator that yields series of consecutive audio frames comprising each
        utterance, separated by yielding None.

        Determines voice activity by ratio of frames in padding_ms. Uses a buffer to
        include padding_ms prior to being triggered.
        Example: (frame, ..., frame, None, None, None, frame, ..., frame, None, ...)
                  |---utterence---|                    |---utterence---|
        """
        num_padding_frames = padding_ms // self.frame_duration_ms
        ring_buffer = collections.deque(maxlen=num_padding_frames)
        ring_buffer2 = collections.deque(maxlen=int(num_padding_frames * silence))

        triggered = False
        for frame in frames:
            if len(frame) < 640:
                yield None
                return

            is_speech = self.vad.is_speech(frame, self.sample_rate)

            if not triggered:
                yield None
                ring_buffer.append((frame, is_speech))
                num_voiced = len([f for f, speech in ring_buffer if speech])
                if num_voiced > ratio * ring_buffer.maxlen:
                    triggered = True
                    for f, s in ring_buffer:
                        yield f
                    ring_buffer.clear()

            else:
                yield frame
                ring_buffer2.append((frame, is_speech))
                num_unvoiced = len([f for f, speech in ring_buffer2 if not speech])
                if num_unvoiced > ratio * ring_buffer2.maxlen:
                    triggered = False
                    yield None
                    ring_buffer2.clear()

    def create_stream(self):
        self.stream_context = bytearray()

    def feed_audio_content(self, chunk):
        assert self.stream_context is not None
        self.stream_context.extend(chunk)

    def finish_stream(self):
        assert self.stream_context is not None
        return np.frombuffer(self.stream_context, np.int16) / 32768


if __name__ == '__main__':
    file_path = "/home/thulx/Downloads/a-Nam-Long.mp3.wav"
    splitter = Splitter(chunk=320, sample_rate=16000, silence=0.4, max_none_frames=2,
                        max_utter=20, min_utter=5)
    sub_wav_list = splitter.split(file_path, do_standardize=True,
                                  save_dir="output")
    assert isinstance(sub_wav_list, list)
    assert isinstance(sub_wav_list[0], np.ndarray)

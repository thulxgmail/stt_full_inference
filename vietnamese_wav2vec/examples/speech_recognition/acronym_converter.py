from typing import Dict, List, Union
import json


class AcronymConverter:
    def __init__(self, acronym2phonetic_path: str):
        self.phonetic2acronym = {
            v: k for k, v in read_json_file(acronym2phonetic_path).items()
        }
        self._process_phonetic2acronym()

    def _process_phonetic2acronym(self):
        self.phonetic2acronym = {
            k.upper(): v for k, v in self.phonetic2acronym.items()
        }
        self.phonetic2acronym = dict(
            sorted(
                self.phonetic2acronym.items(),
                key=lambda x: len(x[0].split("_")),
                reverse=True
            )
        )

    def transform(self, inputs: Union[str, List[str]]):
        if isinstance(inputs, str):
            inputs = [inputs]
        outputs = []
        for sent in inputs:
            word_list = sent.strip().split(" ")
            for phonetic in self.phonetic2acronym.keys():
                sub_phonetic_list = phonetic.split("_")
                if is_sub(sub_phonetic_list, word_list):
                    sent = sent.replace(
                        phonetic.replace("_", " "), self.phonetic2acronym[phonetic]
                    )
            outputs.append(sent)
        if len(outputs) == 1:
            return outputs[0]
        return outputs


def read_json_file(json_file_path: str) -> Dict:
    """Reads json file."""
    with open(json_file_path) as f:
        data = json.load(f)
    return data


def is_sub(sub: List, lst: List) -> bool:
    """Check a list is a subset of another list in order."""
    ln = len(sub)
    for i in range(len(lst) - ln + 1):
        if all(sub[j] == lst[i+j] for j in range(ln)):
            return True
    return False


if __name__ == '__main__':
    test_sentences = [
        "VI EN PI TI PÂY ĐÃ CÓ GẦN NĂM MƯƠI NGHÌN ĐIỂM CHẤP NHẬN THANH TOÁN .",
        "VIỆT THEO CŨNG ĐƯỢC ĐẠT KẾT QUẢ CHẤT LƯỢNG TỐT .",
        "NHÀ MẠNG ÉP PI TI TE LE COM CUNG CẤP CÁC GÓI CƯỚC CHO CÁ NHÂN .",
        "TRÌNH ĐẠI HỘI MƯỜI BA CỦA ĐẢNG VÀ DỰ THẢO BÁO CÁO CHÍNH TRỊ"
    ]
    expected_outputs = [
        "VNPT PAY ĐÃ CÓ GẦN NĂM MƯƠI NGHÌN ĐIỂM CHẤP NHẬN THANH TOÁN .",
        "VIETTEL CŨNG ĐƯỢC ĐẠT KẾT QUẢ CHẤT LƯỢNG TỐT .",
        "NHÀ MẠNG FPT TELECOM CUNG CẤP CÁC GÓI CƯỚC CHO CÁ NHÂN .",
        "TRÌNH ĐẠI HỘI MƯỜI BA CỦA ĐẢNG VÀ DỰ THẢO BÁO CÁO CHÍNH TRỊ"
    ]
    acronym_converter = AcronymConverter("data/acronym_reduce.json")
    converter_outputs = acronym_converter.transform(test_sentences)
    assert expected_outputs == converter_outputs

    test_sentence = "ĐIỂN HÌNH NHẤT LÀ GIẢI PHÁP HỌC TRỰC TUYẾN VI EN PI TI I LƠN NINH"
    expected_output = "ĐIỂN HÌNH NHẤT LÀ GIẢI PHÁP HỌC TRỰC TUYẾN VNPT E-LEARNING"
    converter_output = acronym_converter.transform(test_sentence)
    assert expected_output == converter_output

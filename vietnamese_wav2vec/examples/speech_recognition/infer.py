#!/usr/bin/env python3 -u
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

"""
Run inference for pre-processed data with a trained model.
"""

import logging
import math
import os
import sys
from tqdm import tqdm

import torch.nn as nn
import torch
from vietnamese_wav2vec.vnwav import checkpoint_utils, options, progress_bar, utils, tasks
from vietnamese_wav2vec.vnwav.data.data_utils import post_process
from vietnamese_wav2vec.examples.speech_recognition.acronym_converter import AcronymConverter


logging.basicConfig()
logging.root.setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def add_asr_eval_argument(parser):
    parser.add_argument("--kspmodel", default=None, help="sentence piece model")
    parser.add_argument(
        "--wfstlm", default=None, help="wfstlm on dictonary output units"
    )
    parser.add_argument(
        "--rnnt_decoding_type",
        default="greedy",
        help="wfstlm on dictonary\
output units",
    )
    parser.add_argument(
        "--lm-weight",
        "--lm_weight",
        type=float,
        default=0.2,
        help="weight for lm while interpolating with neural score",
    )
    parser.add_argument(
        "--rnnt_len_penalty", default=-0.5, help="rnnt length penalty on word level"
    )
    parser.add_argument(
        "--w2l-decoder", choices=["viterbi", "kenlm", "fairseqlm"], help="use a w2l decoder"
    )
    parser.add_argument("--lexicon", help="lexicon for w2l decoder")
    parser.add_argument("--unit-lm", action='store_true', help="if using a unit lm")
    parser.add_argument("--kenlm-model", "--lm-model", help="lm model for w2l decoder")
    parser.add_argument("--beam-threshold", type=float, default=25.0)
    parser.add_argument("--beam-size-token", type=float, default=100)
    parser.add_argument("--word-score", type=float, default=1.0)
    parser.add_argument("--unk-weight", type=float, default=-math.inf)
    parser.add_argument("--sil-weight", type=float, default=0.0)
    parser.add_argument(
        "--dump-emissions",
        type=str,
        default=None,
        help="if present, dumps emissions into this file and exits",
    )
    parser.add_argument(
        "--dump-features",
        type=str,
        default=None,
        help="if present, dumps features into this file and exits",
    )
    parser.add_argument(
        "--load-emissions",
        type=str,
        default=None,
        help="if present, loads emissions from this file",
    )
    return parser

def my_make_parser():
    parser = make_parser()
    parser.add_argument("--denoise", help="denoise audio", default=False, type=bool)
    parser.add_argument("--language_vn", help="english or vietnamese", default='vietnamese')
    parser.add_argument("--base_dir_vn", default="demo")
    parser.set_defaults(task="audio_pretraining",
                        nbest=1,
                        gen_subset="train",
                        results_path="infer_test",
                        w2l_decoder="kenlm",
                        lm_weight=1.5,
                        word_score=2,
                        sil_weight=0.0,
                        criterion="ctc",
                        max_sentences=1,
                        post_process="letter",
                        beam=2000,
                        beam_size_token=20)
    parser.add_argument("--chunk", default=320)
    parser.add_argument("--idfile", default=0)
    parser.add_argument("--maxutter", default=5)
    parser.add_argument("--minutter", default=2)
    parser.add_argument("--maxnoneframes", default=2)
    parser.add_argument("--ARGSvad_aggressiveness", default=0)
    parser.add_argument("--ARGSsavewav", default=None)
    parser.add_argument("--ARGSrate", default=16000)
    parser.add_argument("--ARGSsilence", default=1.5)
    parser.add_argument("--ARGSfile", default=None)
    parser.add_argument("--denoise_model_path")
    return parser

def my_parse_args(parser):
    args = options.parse_args_and_arch(parser)
    args.labels = 'ltr'
    args.remove_bpe = 'letter'
    args.denoise_model_path = os.path.join(args.base_dir_vn, "denoise_v1.0.h5")

    if args.language_vn == 'vietnamese':
        args.data = os.path.join(args.base_dir_vn, "data/asset_vn")
        args.path = os.path.join(args.base_dir_vn, "s2t_v1.4.pt")
        args.kenlm_model = os.path.join(args.base_dir_vn, "lm_v1.2.bin")
        args.lexicon = os.path.join(args.base_dir_vn, "lexicon_v1.2.2.txt")
    elif args.language_vn == 'english':
        args.data = os.path.join(args.base_dir_vn, "data/asset")
        args.path = os.path.join(args.base_dir_vn, "asr2_reduce.pt")
        args.kenlm_model = os.path.join(args.base_dir_vn, "4-gram.bin")
        args.lexicon = os.path.join(args.base_dir_vn, "librispeech_lexicon.lst")
    else:
        print('Only english or vietnamese speech to text model is supported so far.')

    args.maxutter = args.maxutter * args.ARGSrate
    args.minutter = args.minutter * args.ARGSrate
    args.maxnoneframes = args.maxnoneframes * args.ARGSrate // args.chunk

    args.acronym_json = os.path.join(args.base_dir_vn, "acronym_v1.1.json")

    print(args.lexicon, args.w2l_decoder, args.kenlm_model)
    print('Use noise suppression:', args.denoise)
    return args


def check_args(args):
    # assert args.path is not None, "--path required for generation!"
    # assert args.results_path is not None, "--results_path required for generation!"
    assert (
            not args.sampling or args.nbest == args.beam
    ), "--sampling requires --nbest to be equal to --beam"
    assert (
            args.replace_unk is None or args.raw_text
    ), "--replace-unk requires a raw text dataset (--raw-text)"


def get_dataset_itr(args, task, models):
    return task.get_batch_iterator(
        dataset=task.dataset(args.gen_subset),
        max_tokens=args.max_tokens,
        max_sentences=args.max_sentences,
        max_positions=(sys.maxsize, sys.maxsize),
        ignore_invalid_inputs=args.skip_invalid_size_inputs_valid_test,
        required_batch_size_multiple=args.required_batch_size_multiple,
        num_shards=args.num_shards,
        shard_id=args.shard_id,
        num_workers=args.num_workers,
        data_buffer_size=args.data_buffer_size,
    ).next_epoch_itr(shuffle=False)


def process_predictions(
        args, hypos, sp, tgt_dict, target_tokens, res_files, speaker, id
):
    best_hypos = []
    for hypo in hypos[: min(len(hypos), args.nbest)]:
        hyp_pieces = tgt_dict.string(hypo["tokens"].int().cpu())

        if "words" in hypo:
            hyp_words = " ".join(hypo["words"])
        else:
            hyp_words = post_process(hyp_pieces, args.remove_bpe)

        # return 272, 1997, (hyp_words, id)

        best_hypos.append(hyp_words)
    score = hypos[0]['score']
    return score, 1997, (best_hypos, id)


def prepare_result_files(args):
    def get_res_file(file_prefix):
        if args.num_shards > 1:
            file_prefix = f'{args.shard_id}_{file_prefix}'
        path = os.path.join(
            args.results_path,
            "{}-{}-{}.txt".format(
                file_prefix, os.path.basename(args.path), args.gen_subset
            ),
        )
        return open(path, "w", buffering=1)

    if not args.results_path:
        return None

    return {
        "hypo.words": get_res_file("hypo.word"),
        "hypo.units": get_res_file("hypo.units"),
        "ref.words": get_res_file("ref.word"),
        "ref.units": get_res_file("ref.units"),
    }


def load_models_and_criterions(filenames, data_path, arg_overrides=None, task=None, model_state=None):
    models = []
    criterions = []

    if arg_overrides is None:
        arg_overrides = {}

    arg_overrides['wer_args'] = None
    arg_overrides['data'] = data_path

    if filenames is None:
        assert model_state is not None
        filenames = [0]
    else:
        filenames = filenames.split(":")

    for filename in filenames:
        if model_state is None:
            if not os.path.exists(filename):
                raise IOError("Model file not found: {}".format(filename))
            state = checkpoint_utils.load_checkpoint_to_cpu(filename, arg_overrides)
        else:
            state = model_state

        args = state["args"]
        if task is None:
            task = tasks.setup_task(args)
        model = task.build_model(args)
        model.load_state_dict(state["model"], strict=True)
        models.append(model)

        criterion = task.build_criterion(args)
        if "criterion" in state:
            criterion.load_state_dict(state["criterion"], strict=True)
        criterions.append(criterion)
    return models, criterions, args


def optimize_models(args, use_cuda, models):
    """Optimize ensemble for generation
    """
    for model in models:
        model.make_generation_fast_(
            beamable_mm_beam_size=None if args.no_beamable_mm else args.beam,
            need_attn=args.print_alignment,
        )
        if args.fp16:
            model.half()
        if use_cuda:
            device = torch.device("cuda:0")
            model = nn.DataParallel(model)
            model.to(device)


def load_model(args, task=None, model_state=None, optionlm=False):
    check_args(args)

    if args.max_tokens is None and args.max_sentences is None:
        args.max_tokens = 4000000
    # logger.info(args)

    use_cuda = torch.cuda.is_available() and not args.cpu

    if task is None:
        # Load dataset splits
        task = tasks.setup_task(args)
        task.load_dataset(args.gen_subset)

    # logger.info("| decoding with criterion {}".format(args.criterion))

    # Load ensemble

    if args.load_emissions:
        models, criterions = [], []
    else:
        # logger.info("| loading model(s) from {}".format(args.path))
        print("loading model(s) from {}".format(args.path))
        models, criterions, _ = load_models_and_criterions(
            args.path,
            data_path=args.data,
            arg_overrides=eval(args.model_overrides),  # noqa
            task=task,
            model_state=model_state,
        )
        optimize_models(args, use_cuda, models)

    # hack to pass transitions to W2lDecoder
    if args.criterion == "asg_loss":
        trans = criterions[0].asg.trans.data
        args.asg_transitions = torch.flatten(trans).tolist()

    def build_generator(args):
        w2l_decoder = getattr(args, "w2l_decoder", None)
        if w2l_decoder == "viterbi":
            from vietnamese_wav2vec.examples.speech_recognition.w2l_decoder import W2lViterbiDecoder

            return W2lViterbiDecoder(args, task.target_dictionary)
        elif w2l_decoder == "kenlm":
            from vietnamese_wav2vec.examples.speech_recognition.w2l_decoder import W2lKenLMDecoder

            return W2lKenLMDecoder(args, task.target_dictionary)
        elif w2l_decoder == "fairseqlm":
            from vietnamese_wav2vec.examples.speech_recognition.w2l_decoder import W2lFairseqLMDecoder

            return W2lFairseqLMDecoder(args, task.target_dictionary)
        else:
            print(
                "only wav2letter decoders with (viterbi, kenlm, fairseqlm) options are supported at the moment"
            )

    if optionlm:
        from vietnamese_wav2vec.examples.speech_recognition.w2l_decoder import W2lViterbiDecoder, W2lKenLMDecoder
        generator1 = W2lViterbiDecoder(args, task.target_dictionary)
        generator2 = W2lKenLMDecoder(args, task.target_dictionary)
        generator = [generator1, generator2]
    else:
        generator = build_generator(args)

    return models, generator

def transcribe(sound_list=None, sr_list=None, args=None, models=None, generator=None, license=None, task=None,
               getsocre=False, acronym_converter=None, predict_file=None, debug=False):
    if license != "VNPT-IT-IC":
        print("Processing normally")
        sys.exit()

    check_args(args)

    if args.max_tokens is None and args.max_sentences is None:
        args.max_tokens = 4000000

    use_cuda = torch.cuda.is_available() and not args.cpu

    if task is None:
        # Load dataset splits
        task = tasks.setup_task(args)
        if not sound_list and not  sr_list:
            task.my_load_dataset(args.gen_subset)
        else:
            task.load_dataset(args.gen_subset, sounds=sound_list, sample_rates=sr_list)

    # Set dictionary
    tgt_dict = task.target_dictionary

    itr = get_dataset_itr(args, task, models)

    hypo_sents = []
    score_list = []
    if not predict_file: predict_file = 'predict.txt'
    device = torch.device("cuda:0")
    with open(predict_file, 'w') as f:
        for sample in tqdm(itr):
            sample = utils.move_to_cuda(sample, device=device) if use_cuda else sample
            if "net_input" not in sample:
                continue

            ids = sample['id']
            source = sample['net_input']['source']
            padmask = sample['net_input']['padding_mask']
            sample = torch.cat((source, padmask), 1)

            hypos = task.inference_step(generator, models, sample, prefix_tokens=None)

            sample = {'id': ids}
            for i, sample_id in enumerate(sample["id"].tolist()):
                # Process top predictions
                score, _, hypo_word_id = process_predictions(
                    args, hypos[i], None, tgt_dict, target_tokens=None, res_files=None, speaker=None, id=sample_id
                )
                sents = hypo_word_id[0]
                if acronym_converter:
                    sents = [acronym_converter.transform(hypo_word_id[0])]
                if debug: print(sents[0])
                f.write(sents[0] + '\n')
                if sound_list:
                    hypo_sents.append(sents[0])
                    score_list.append(score)


    if getsocre:
        return hypo_sents, score_list
    return hypo_sents


class Infer():
    def __init__(self, args, model, generator):
        self.args = args
        self.model = model
        self.generator = generator
        self.acronym_converter = AcronymConverter(args.acronym_json)

    def predict(self, sound_list=None, sr_list=None, license=None, convert_acronym=True, optionlm=False, uselm=True,
                predict_file=None, debug=False):
        acronym_converter = self.acronym_converter if convert_acronym else None
        if optionlm:
            if uselm:
                hypo_sents = transcribe(sound_list=sound_list, sr_list=sr_list, args=self.args,
                                        models=self.model, generator=self.generator[1], license=license,
                                        acronym_converter=acronym_converter, predict_file=predict_file, debug=debug)
            else:
                hypo_sents = transcribe(sound_list=sound_list, sr_list=sr_list, args=self.args,
                                        models=self.model, generator=self.generator[0], license=license,
                                        acronym_converter=acronym_converter, predict_file=predict_file, debug=debug)
        else:
            hypo_sents =  transcribe(sound_list=sound_list, sr_list=sr_list, args=self.args,
                                     models=self.model, generator=self.generator, license=license,
                                     acronym_converter=acronym_converter, predict_file=predict_file, debug=debug)
        return hypo_sents


def make_parser():
    parser = options.get_generation_parser()
    parser = add_asr_eval_argument(parser)
    return parser


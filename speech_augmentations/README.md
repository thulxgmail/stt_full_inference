# Setup
## Requirements
* python == 3.8
* torch == 1.8.1
* torchaudio == 0.8.1
* WavAugment
## Install WavAugment
```bash
git clone https://github.com/facebookresearch/WavAugment.git
cd WavAugment
pip install -e .
```
# Usage
Xem ví dụ tại hàm `main` của file `augmentors.py`.

Class `AudioAugmentation` kết hợp 3 phương pháp augment đó là pitch shift, band reject 
filter và reverberation. Cả 3 phương pháp này đều có tham số riêng để custom.

Cả 3 phương pháp đều có thể nhận tham số là các hàm random để làm tăng thêm độ phong
phú cho data (các lần thực hiện augment sẽ cho ra kết quả khác nhau), xem thêm class 
`RandomPitchShift`, `RandomReverb`, `SpecAugmentBand`.

## Thực thi ví dụ
```bash
cd speech_augmentations
python augmentors.py
```
Ví dụ mô phỏng 10 lần thực hiện augment một sample audio. Ouput được lưu tại
`augmentations/data/augmentation`
import os

from augment import EffectChain
import numpy as np
import torch
import torchaudio


class RandomPitchShift:
    """Creates a random pith shift value.

    Attributes:
        shift_max: Amplitude of a pitch shift; measured in 1/100th of a tone.
    """
    def __init__(self, shift_max: int = 300):
        self.shift_max = shift_max

    def __call__(self):
        return np.random.randint(-self.shift_max, self.shift_max)


class RandomReverb:
    """ Create random reverberation values reverberance, dumping factor, and
    room size.

    Attributes:
        reverberance_min: Minimal reverberance used in randomized reverb (0..100).
        reverberance_max: Maximal reverberance used in randomized reverb (0..100).
        damping_min: Minimal damping used in randomized reverb (0..100).
        damping_max: Maximal damping used in randomized reverb (0..100).
        room_scale_min: Minimal room size used in randomized reverb (0..100).
        room_scale_max: Maximal room size used in randomized reverb (0..100).
    """
    def __init__(
            self,
            reverberance_min: int = 25,
            reverberance_max: int = 25,
            damping_min: int = 25,
            damping_max: int = 25,
            room_scale_min: int = 0,
            room_scale_max: int = 20
    ):
        self.reverberance_min = reverberance_min
        self.reverberance_max = reverberance_max
        self.damping_min = damping_min
        self.damping_max = damping_max
        self.room_scale_min = room_scale_min
        self.room_scale_max = room_scale_max

    def __call__(self):
        reverberance = np.random.randint(
            self.reverberance_min, self.reverberance_max + 1
        )
        damping = np.random.randint(self.damping_min, self.damping_max + 1)
        room_scale = np.random.randint(self.room_scale_min, self.room_scale_max + 1)

        return [reverberance, damping, room_scale]


class SpecAugmentBand:
    def __init__(self, sampling_rate: int, scaler: float):
        self.sampling_rate = sampling_rate
        self.scaler = scaler

    def __call__(self):
        F = 27.0 * self.scaler
        melfmax = freq2mel(self.sampling_rate / 2)
        meldf = np.random.uniform(0, melfmax * F / 256.)
        melf0 = np.random.uniform(0, melfmax - meldf)
        low = mel2freq(melf0)
        high = mel2freq(melf0 + meldf)
        return f"{high}-{low}"


class AudioAugmentation:
    """
    Creates an instance of augment.EffectChain and applies it on pytorch tensors.

    Attributes:
        sample_rate: sampling rate of input sequence.
        length: length of input sequence.
        channels: number of channels of input sequence.
    """

    def __init__(self, sample_rate: int, length: int, channels: int):
        self.sample_rate = sample_rate
        self.length = length
        self.channels = channels
        self.max_pith_shift = 300
        self.reverberance_min = 20
        self.reverberance_max = 30
        self.damping_min = 20
        self.damping_max = 30
        self.room_scale_min = 0
        self.room_scale_max = 25
        self.band_reject_scaler = 2
        self.augment_pipeline = self._build_chain()

    def _build_chain(self):
        augment_pipeline = EffectChain()
        # applies band reject filter.
        augment_pipeline = augment_pipeline.sinc(
            "-a", "120", SpecAugmentBand(self.sample_rate, self.band_reject_scaler)
        )
        # applies pitch shift.
        augment_pipeline = augment_pipeline.pitch(
            "-q", RandomPitchShift(self.max_pith_shift)
        ).rate("-q", self.sample_rate)
        # applies reverberation.
        augment_pipeline = augment_pipeline.reverb(
            RandomReverb(
                reverberance_min=self.reverberance_min,
                reverberance_max=self.reverberance_max,
                damping_min=self.damping_min,
                damping_max=self.damping_max,
                room_scale_min=self.room_scale_min,
                room_scale_max=self.room_scale_max
            )
        ).channels()
        return augment_pipeline

    def __call__(self, x):
        """
        x: torch.Tensor, (channels, length). Must be placed on CPU.
        """
        src_info = {
            "channels": self.channels,
            "length": self.length,
            "rate": self.sample_rate,
        }

        target_info = {
            "channels": 1,
            "length": self.length,
            "rate": self.sample_rate,
        }

        y = self.augment_pipeline.apply(
            x, src_info=src_info, target_info=target_info)

        # sox might misbehave sometimes by giving nan/inf if sequences are too short
        # (or silent) and the effect chain includes eg `pitch`.
        if torch.isnan(y).any() or torch.isinf(y).any():
            return x.clone()
        return y


def freq2mel(f):
    return 2595. * np.log10(1 + f / 700)


def mel2freq(m):
    return (10. ** (m / 2595.) - 1) * 700


if __name__ == '__main__':
    data_path = "data"
    augmentation_path = os.path.join(data_path, "augmentation")
    sample_audio_name = "6b7e637f7c7e4e34ac053fc84b8d11ef"
    n_epoch = 10

    wav, sample_rate = torchaudio.load(
        os.path.join(data_path, f"{sample_audio_name}.wav")
    )
    if not os.path.isdir(augmentation_path):
        os.mkdir(augmentation_path)

    augmentation_pipeline = AudioAugmentation(
        sample_rate=sample_rate, length=wav.size(0), channels=wav.size(1)
    )
    for epoch in range(n_epoch):
        augmented_save_name = f"{sample_audio_name}_augment_{epoch}.wav"
        augmented_wav = augmentation_pipeline(wav)
        torchaudio.save(
            os.path.join(augmentation_path, augmented_save_name),
            augmented_wav,
            sample_rate
        )

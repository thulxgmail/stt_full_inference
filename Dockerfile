FROM wav2letter/wav2letter:cuda-latest
LABEL maintainer="LXT"

# Environment
ENV LANG=en_US.utf8
ENV LANG C.UTF-8

# Variables
# Install basic packages and miscellaneous dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    cmake \
    wget \
    curl \
    bzip2 \
    vim \
    ffmpeg \
    unzip \
    alien \
    libaio1\
    libsm6 libxext6 libxrender-dev\
    git \
    portaudio19-dev \
    && curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \
    && bash /tmp/miniconda.sh -bfp /usr/local \
    && rm -rf /tmp/miniconda.sh \
    && apt-get clean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*
RUN apt update -y
## cpu backend
#Run /bin/bash -c "export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2018.5.274/linux/mkl/lib/intel64:$LD_IBRARY_PATH"
## "export USE_CUDA=0"
#ENV USE_CUDA=0
# Create env
RUN conda clean --all --yes
RUN conda create -n vnpt python=3.6
Run echo "source activate vnpt" > ~/.bashrc
ENV PATH /opt/conda/envs/vnpt/bin:$PATH
#Run /bin/bash -c  "source activate vnpt && \
#conda install -c anaconda tensorflow-gpu==2.2.0"
# install pytorch, packaging
Run /bin/bash -c  "source activate vnpt && \
pip install torch==1.6.0 && \
pip install packaging"
# wav2vec's requirements
COPY stt_full_inference/requirements.txt /requirements.txt
Run /bin/bash -c "source activate vnpt && \
pip install -r requirements.txt"
#
ADD stt_full_inference /stt_full_inference
WORKDIR /stt_full_inference
# install kenlm
Run /bin/bash -c "git clone https://github.com/kpu/kenlm && \
cd kenlm && mkdir -p build && cd build && cmake .. && make -j 4"
#export KENLM_ROOT_DIR=/wav2letter_python_bindings/kenlm"
ENV KENLM_ROOT_DIR=/stt_full_inference/kenlm
# install wav2letter python bindings
Run /bin/bash -c "source activate vnpt && \
git clone -b v0.2 https://github.com/facebookresearch/wav2letter.git && \
cd wav2letter && \
git checkout 96f5f9 && \
cd bindings/python && \
pip install -e ."
# install wav2vec
Run /bin/bash -c "source activate vnpt && \
pip install -e ."


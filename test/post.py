import numpy as np
from api import t2s

if __name__ == '__main__':
    print("-----go-------")

    texts = '/media/thulx/HDD/data/speech/ASR/correction/acronym/step2/acronym_data_2.txt_clean2.txt'
    save_dir = '/media/thulx/HDD/data/speech/ASR/correction/acronym/step2/audio'
    models = ['books', 'news']
    regions = ['female_north', 'female_central', 'female_south',
                'male_south']
    # regions = ['male_north']
    with open(texts, 'r') as ft:
        for i, text in enumerate(ft):
            # if i < 4417:
            #     continue
            model = np.random.choice(models)
            region = np.random.choice(regions)

            while True:
                try:
                    x = t2s(i, text[:-1], model, region, save_dir=save_dir)
                    if x:
                        break
                except:
                    print('lost connection, retry...')
            print(i)




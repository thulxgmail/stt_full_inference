import requests, os

def t2s(id, text, model, region, save_dir ):
    if text == '':
        return
    task = {
        "text": text,
        "text_split": False,
        "model": model,
        "region": region
    }
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsicmVzdHNlcnZpY2UiXSwidXNlcl9uYW1lIjoiY29uZ0B0ZXN0LmNvbSIsImF1dGhvcml0aWVzIjpbIlVTRVIiXSwianRpIjoiYjQxZTM4NmUtMTM2OS00OGUwLWEyNWYtYmYyNDk4YjQ0Zjc2IiwiY2xpZW50X2lkIjoiYWRtaW5hcHAiLCJzY29wZSI6WyJyZWFkIl19.Bt3b8R1ODwGPW7PeUY3q7qWGfwj5rOmUqdvH9216u1_9S_Nt4kWTDtmAMkIlKCoXr-KKygTQOcEZsUEuHRBVqmG3vtuaxicJ_fcaljXSmTPVW1m6jL8It6i96VzuCOrQ_LhdS0Ta8SbUcbIdQVpjjpuCqAVry09Ccfm9LZeOJCWB1gksI43XhKZkvk8QXYc-0uNsTOMIkJfGu_cc1rZ6NIweSiy_gO6UuBU8dHxIAyc3tr-vn9-AfGmcQ1iq-225Uh5ZiDKLFQ7sfStmbTxPdUfyUvq1EDmojn2I_vu_sygBzoix1EI7oEPjWI7OcA_iyRvelWYIBrhRxQW-VoatTQ',
               'Token-id': 'b8d8d100-de7f-7647-e053-6c1b9f0a2918',
               'Token-key': 'MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIqkxSSnHu7nPx/kc8xLEajo0NxB6VzA1st3iPeBtd39g54R1xe6kkaqaOaoRYn/f1vHP0480qizKc3isFVUtlkCAwEAAQ==',
               'mac-address': '00-14-22-01-23-45-15'}
    # print('d1')
    check1 = True
    while check1:
        resp = requests.post('https://explorer.idg.vnpt.vn/voice-service/text-to-speech', json=task, headers=headers,
                             timeout=0.5)
        if resp.status_code == 200:
            check1 = False
    # print('d2')
    task_download = {
      "text_id" : resp.json()['object']['text_id'],
      "type" : "wav"
    }
    # print('d3')
    check1 = True
    while check1:
        resp2 = requests.post('https://explorer.idg.vnpt.vn/voice-service/text-to-speech/download', json=task_download,
                              headers=headers, timeout=0.5)
        if resp2.status_code == 200:
            check1 = False
    # print('d4')
    # while resp2.status_code != 200:
    #     resp2 = requests.post('https://explorer.idg.vnpt.vn/voice-service/text-to-speech/download', json=task_download,
    #                           headers=headers, timeout=2)
    #     print(resp2.status_code)
    # print('d5')

    fn = '_'.join([str(id).zfill(5), model, region]) + '.wav'
    with open(os.path.join(save_dir, fn), 'wb') as f:
        f.write(resp2.content)

    return True
